<?php
	/**
	 * 
	 */
	class Curlcall 	
	{
		public static function make_curl_call($url='',$headers=[],$method='GET',$post_params=[])
		{
			$curl = curl_init();
	        curl_setopt_array($curl, array(
	            CURLOPT_URL => $url,
	            CURLOPT_RETURNTRANSFER => true,
	            CURLOPT_ENCODING => '',
	            CURLOPT_MAXREDIRS => 10,
	            CURLOPT_TIMEOUT => 0,
	            CURLOPT_FOLLOWLOCATION => true,
	            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	            CURLOPT_CUSTOMREQUEST => $method,
	            CURLOPT_HTTPHEADER => $headers,
	            ),
	        );
	        $response = (curl_exec($curl));
	        curl_close($curl);

	        $temp_data = json_decode($response);
	        if(empty($response) || (isset($temp_data->Title) && ($temp_data->Title == 'Unauthorized' || $temp_data->Title == 'Forbidden')))
	        {
	        	echo '<pre>';
	        	echo 'Something went wrong.<br>';
	        	unset($_SESSION['access_token']);
	        	unset($_SESSION['refresh_token']);
	        	print_r($temp_data);
	        	die;
	        }
	        else
	        {
	        	return $response;	
	        }
		}
	}
?>