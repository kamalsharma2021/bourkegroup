<?php
    require("config/config.php");
    require("utilities/Curlcall.php");
    $ScriptStartTime = microtime(true);
    $ReportDate = '2021-07-01';
    $FromDate = '2022-02-01';
    $ToDate = '2022-02-28';
    $DateRangeArray = date_range($FromDate,$ToDate,'+1 day','Y-m-d');
    $MasterFlagArray = array('ClientGroupUpdate'=>false,'AccountsUpdate'=>false,'BudgetsUpdate'=>false,'AccountsBudgetUpdate'=>false,'ProfitLoss'=>false,'ClientGroupProfitLossUpdate'=>false);

    if(strtotime($FromDate)>strtotime($ToDate))
    {
        echo 'Invalid date range<br>From-date can not be after To-date';
        die;
    }
    else if(count($DateRangeArray)>366)
    {
        echo 'Please keep date range upto 1year/366 days, more range will lead to take more time to compute & fail to script.';
        die;
    }
    
    #start client_group management---------------------
    if($MasterFlagArray['ClientGroupUpdate'] === true)
    {
        $url = $base_xero_url.'TrackingCategories/';
        $method = 'GET';
        $headers = get_curl_headers();
        $ClientGroupResponse = Curlcall::make_curl_call($url,$headers,$method);

        $ClientGroupData = json_decode($ClientGroupResponse);
        if(isset($ClientGroupData->TrackingCategories))
        {
            $ClientGroupDataArray = $ClientGroupData->TrackingCategories;
            foreach($ClientGroupDataArray as $k => $v)
            {
                if($v->Name == 'Client Groups' && !empty($v->Options))
                {   
                    foreach($v->Options as $k1 => $v1)
                    {
                        #cross check if this client_group code already exist--------
                        $q = "SELECT * FROM client_group WHERE TrackingOptionID = '".$v1->TrackingOptionID."'";
                        $r = sqlsrv_query($conn,$q,array(), array("Scrollable" => 'static'));
                        if(sqlsrv_num_rows($r)>0)
                        {   
                            // $d = sqlsrv_fetch_array($r);  
                            // #update their parent headings------------
                            // if($d['ParentHeadingID'] == 0)
                            // {
                            //     $q1 = "UPDATE accounts SET ParentHeadingID='".$ParentHeadingID."' WHERE Id = '".$d['Id']."'";
                            //     sqlsrv_query($conn,$q1);   
                            // }
                            // else if($d['Name'] != $v->Name)
                            // {
                            //     #update name if changed--------
                            //     $q1 = "UPDATE accounts SET Name='".$v->Name."' WHERE Id = '".$d['Id']."'";
                            //     sqlsrv_query($conn,$q1);   
                            // }
                        }
                        else
                        {
                            $q1 = "INSERT INTO client_group(Name,TrackingOptionID, TrackingCategoryID, IsActive) VALUES ('".mysql_real_escape_string($v1->Name)."','".$v1->TrackingOptionID."','".$v->TrackingCategoryID."','".$v1->IsActive."')";
                            $r1 = sqlsrv_query($conn,$q1);  
                            if($r1 === false)
                            {
                                echo "something went wrong.<br>".$q1;
                                die;
                            }

                        }
                    }
                }                
            }
        }   
    }
    #end client_group management---------------------


    #start accounts management---------------------
    if($MasterFlagArray['AccountsUpdate'] === true)
    {
        $url = $base_xero_url.'Accounts';
        $method = 'GET';
        $headers = get_curl_headers();
        $AccountResponse = Curlcall::make_curl_call($url,$headers,$method);

        $AccountData = json_decode($AccountResponse);
        if(isset($AccountData->Accounts))
        {
            $AccountDataArray = $AccountData->Accounts;
            foreach($AccountDataArray as $k => $v)
            {
                $Code = isset($v->Code)?$v->Code:Null;
                $Description = isset($v->Description)?$v->Description:Null;
                $ParentHeadingID = 0;
                if($v->Class == 'REVENUE'){$ParentHeadingID = 1;}
                elseif($v->Class == 'EXPENSE'){$ParentHeadingID = 2;}
                #cross check if this account code already exist--------
                $q = "SELECT * FROM accounts WHERE AccountID = '".$v->AccountID."'";
                $r = sqlsrv_query($conn,$q,array(), array("Scrollable" => 'static'));
                if(sqlsrv_num_rows($r)>0)
                {   
                    $d = sqlsrv_fetch_array($r);  
                    #update their parent headings------------
                    if($d['ParentHeadingID'] == 0)
                    {
                        $q1 = "UPDATE accounts SET ParentHeadingID='".$ParentHeadingID."' WHERE Id = '".$d['Id']."'";
                        sqlsrv_query($conn,$q1);   
                    }
                    else if($d['Name'] != $v->Name)
                    {
                        #update name if changed--------
                        $q1 = "UPDATE accounts SET Name='".$v->Name."' WHERE Id = '".$d['Id']."'";
                        sqlsrv_query($conn,$q1);   
                    }
                }
                else
                {
                    $SystemAccount = (!empty($v->SystemAccount))?$v->SystemAccount:Null;
                    $q1 = "INSERT INTO accounts(AccountID,ParentHeadingID, Name, Status, Type, TaxType, Class, SystemAccount, Code, Description,AddOn) VALUES ('".$v->AccountID."','".$ParentHeadingID."','".$v->Name."','".$v->Status."','".$v->Type."','".$v->TaxType."','".$v->Class."','".$SystemAccount."','".$Code."','".addslashes($Description)."',GETDATE())";
                    sqlsrv_query($conn,$q1);  
                }
            }
        }   
    }
    #end accounts management---------------------



    #start budgets management---------------------
    if($MasterFlagArray['BudgetsUpdate'] === true)
    {
        $url = $base_xero_url.'Budgets';
        $method = 'GET';
        $headers = get_curl_headers();
        $BudgetResponse = Curlcall::make_curl_call($url,$headers,$method);
        
        $BudgetData = json_decode($BudgetResponse);
        if(isset($BudgetData->Budgets))
        {
            $BudgetDataArray = $BudgetData->Budgets;
            foreach($BudgetDataArray as $k => $v)
            {
                $Description = isset($v->Description)?$v->Description:Null;

                #cross check if this budget already exist--------
                $q = "SELECT * FROM budgets WHERE BudgetID = '".$v->BudgetID."'";
                $r = sqlsrv_query($conn,$q,array(), array("Scrollable" => 'static'));
                if(sqlsrv_num_rows($r)>0)
                {   
                    $d = sqlsrv_fetch_array($r);  
                    #update their status------------
                    if($d['Status'] != $v->Status)
                    {
                        #update name if changed--------
                        $q1 = "UPDATE budgets SET Status='".$v->Status."' WHERE Id = '".$d['Id']."'";
                        sqlsrv_query($conn,$q1);   
                    }
                }
                else
                {
                    $q1 = "INSERT INTO budgets(BudgetID, Status, Description, Type, UpdatedDateUTC, AddOn) VALUES ('".$v->BudgetID."','".$v->Status."','".addslashes($Description)."','".$v->Type."','".xero_date($v->UpdatedDateUTC)."',GETDATE())";
                    sqlsrv_query($conn,$q1);   
                }
            }
        }
    }
    #end budgets management---------------------


    #start accounts_budget management---------------------
    if($MasterFlagArray['AccountsBudgetUpdate'] === true)
    {
        $StartDate = '2021-07-01';
        $EndDate = '2022-06-30';
        $LocalAccountArray = array();
        $LocalBudgetArray = array();

        $q = 'SELECT Id,BudgetID FROM Budgets';
        $r = sqlsrv_query($conn,$q);
        while($d = sqlsrv_fetch_array($r))
        {
            $LocalBudgetArray[$d['BudgetID']] = $d['Id'];
        }

        $q = 'SELECT AccountID,Code,Id FROM accounts';
        $r = sqlsrv_query($conn,$q);
        while($d = sqlsrv_fetch_array($r))
        {
            $LocalAccountArray[$d['AccountID']] = $d['Id'];
        }

        if(!empty($LocalBudgetArray))
        {
            foreach($LocalBudgetArray as $k1 => $v1)
            {
                $url = $base_xero_url.'Budgets/'.$k1.'?DateFrom='.$StartDate.'&DateTo='.$EndDate;
                $method = 'GET';
                $headers = get_curl_headers();
                $AccountsBudgetResponse = Curlcall::make_curl_call($url,$headers,$method);

                $AccountsBudgetData = json_decode($AccountsBudgetResponse);
                if(isset($AccountsBudgetData->Budgets['0']->BudgetLines))
                {
                    $AccountsBudgetDataArray = $AccountsBudgetData->Budgets['0']->BudgetLines;
                    foreach($AccountsBudgetDataArray as $k => $v)
                    {
                        foreach($v->BudgetBalances as $k2 => $v2)
                        {
                            $TempMonth = date('m',strtotime($v2->Period));
                            $TempYear = date('Y',strtotime($v2->Period));
                            #cross check if this accounts_budget already exist--------
                            $q = "SELECT Id FROM accounts_budget WHERE BudgetID = '".$v1."' AND AccountID = '".$LocalAccountArray[$v->AccountID]."' AND Month = '".$TempMonth."' AND Year = '".$TempYear."'";
                            $r = sqlsrv_query($conn,$q,array(), array("Scrollable" => 'static'));
                            if(sqlsrv_num_rows($r)>0)
                            {   
                                $d = sqlsrv_fetch_array($r);  
                                $q1 = "UPDATE accounts_budget SET Value='".$v2->Amount."' WHERE Id = '".$d['Id']."'";
                                sqlsrv_query($conn,$q1);                               
                            }
                            else
                            {
                                $q1 = "INSERT INTO accounts_budget(BudgetID, Month, Year, AccountID, Value, AddOn) VALUES ('".$v1."','".$TempMonth."','".$TempYear."','".$LocalAccountArray[$v->AccountID]."','".$v2->Amount."',GETDATE())";
                                sqlsrv_query($conn,$q1);   
                            }
                        }
                    }
                }   
            }   
        }
    }
    #end accounts_budget management---------------------

    #start profit_loss management---------------------
    if($MasterFlagArray['ProfitLoss'] === true)
    {
        if(!empty($DateRangeArray))
        {
            foreach($DateRangeArray as $k => $v)
            {
                #fetch everyday data from xero api---------
                $url = $base_xero_url.'Reports/ProfitAndLoss?fromDate='.$v.'&toDate='.$v.'&standardLayout=true&periods=1';
                $method = 'GET';
                $headers = get_curl_headers();
                $ProfitLossResponse = Curlcall::make_curl_call($url,$headers,$method);

                $ProfitLossData = json_decode($ProfitLossResponse);
                if(isset($ProfitLossData->Reports[0]->Rows))
                {
                    $ProfitLossDataArray = $ProfitLossData->Reports[0]->Rows;
                    
                    #temp array of all accounts value in pair of key=>value to optimize code--------
                    $ProfitLossDataArrayTemp = array();
                    foreach($ProfitLossDataArray as $k1 => $v1)
                    {
                        if(!empty($v1->Title))
                        {
                            $SectionDataArray = $v1->Rows;
                            foreach($SectionDataArray as $k2 => $v2) 
                            {
                                $AccountID = isset($v2->Cells[0]->Attributes[0]->Value)?$v2->Cells[0]->Attributes[0]->Value:'';
                                $Value = $v2->Cells[1]->Value;
                                if(!empty($AccountID))
                                {
                                    $q1 = "SELECT Id FROM accounts WHERE AccountID = '".$AccountID."'";
                                    $r1 = sqlsrv_query($conn,$q1,array(), array("Scrollable" => 'static'));
                                    if(sqlsrv_num_rows($r1)>0)
                                    {
                                        $d1 = sqlsrv_fetch_array($r1);
                                        $ProfitLossDataArrayTemp[$d1['Id']] = $Value;
                                    }
                                }
                            }
                        }
                    }

                    if(count($ProfitLossDataArrayTemp)>0)
                    {
                        foreach($ProfitLossDataArrayTemp as $k1 => $v1) 
                        {
                            #cross check if this accounts_value already exist--------
                            $q = "SELECT Id,Value FROM profit_loss WHERE AccountID = '".$k1."' AND Date = '".$v."'";
                            $r = sqlsrv_query($conn,$q,array(), array("Scrollable" => 'static'));
                            if(sqlsrv_num_rows($r)>0)
                            {   
                                $d = sqlsrv_fetch_array($r);  
                                #update their value------------
                                if($d['Value'] != $v1)
                                {
                                    #update value if changed--------
                                    $q2 = "UPDATE profit_loss SET Value='".$v1."' UpdateOn = GETDATE() WHERE Id = '".$d['Id']."'";
                                    sqlsrv_query($conn,$q2);   
                                }
                            }
                            else
                            {
                                $q2 = "INSERT INTO profit_loss(AccountID, Value, Date, AddOn) VALUES ('".$k1."','".$v1."','".$v."',GETDATE())";
                                sqlsrv_query($conn,$q2);   
                            }
                        }
                    }
                }   
            }   
        }
    }
    #end profit_loss management---------------------


    #start client_group_profit_loss management---------------------
    if($MasterFlagArray['ClientGroupProfitLossUpdate'] === true)
    {
        if(!empty($DateRangeArray))
        {
            $XeroCurlCount = 0;
            $q4 = "SELECT * FROM client_group WHERE (IsActive = '1' AND Id > '0')";
            $r4 = sqlsrv_query($conn,$q4,array(), array("Scrollable" => 'static'));
            if(sqlsrv_num_rows($r4)>0)
            {
                while($d4 = sqlsrv_fetch_array($r4))
                {
                    foreach($DateRangeArray as $k => $v)
                    {
                        $XeroCurlCount++;
                        $TimeNow = microtime(true);
                        $ExecutionTime = (round(($ScriptStartTime-$TimeNow),2)/60); #in minutes------
                        echo 'Curl Count is'.$XeroCurlCount.' Time'.$ExecutionTime.'<br><br>';
                        
                        if(($ExecutionTime>=25 && ($ExecutionTime%25) == 0)) #refresh token with-in each 30min-------
                        {
                            reset_xero_token();
                            echo '<br>reset toekn when curl count is'.$XeroCurlCount.' Time'.$ExecutionTime.'<br><br>';
                        }
                        else if(($XeroCurlCount%55) == 0) #make sure batch of 55 call in a row & then sleep--------
                        {
                            sleep(50);#sleep for 50 seconds------
                            echo '<br>script sleep when curl count is'.$XeroCurlCount.' Time'.$ExecutionTime.'<br><br>';
                        }



                        #fetch everyday data from xero api---------
                        $url = $base_xero_url.'Reports/ProfitAndLoss?fromDate='.$v.'&toDate='.$v.'&standardLayout=true&periods=1&trackingCategoryID='.$d4['TrackingCategoryID'].'&trackingOptionID='.$d4['TrackingOptionID'];
                        $method = 'GET';
                        $headers = get_curl_headers();
                        $ProfitLossResponse = Curlcall::make_curl_call($url,$headers,$method);

                        $ProfitLossData = json_decode($ProfitLossResponse);
                        if(isset($ProfitLossData->Reports[0]->Rows))
                        {
                            $ProfitLossDataArray = $ProfitLossData->Reports[0]->Rows;
                            
                            #temp array of all accounts value in pair of key=>value to optimize code--------
                            $ProfitLossDataArrayTemp = array();
                            foreach($ProfitLossDataArray as $k1 => $v1)
                            {
                                if(!empty($v1->Title))
                                {
                                    $SectionDataArray = $v1->Rows;
                                    foreach($SectionDataArray as $k2 => $v2) 
                                    {
                                        $AccountID = isset($v2->Cells[0]->Attributes[0]->Value)?$v2->Cells[0]->Attributes[0]->Value:'';
                                        $Value = $v2->Cells[1]->Value;
                                        if(!empty($AccountID))
                                        {
                                            $q1 = "SELECT Id FROM accounts WHERE AccountID = '".$AccountID."'";
                                            $r1 = sqlsrv_query($conn,$q1,array(), array("Scrollable" => 'static'));
                                            if(sqlsrv_num_rows($r1)>0)
                                            {
                                                $d1 = sqlsrv_fetch_array($r1);
                                                $ProfitLossDataArrayTemp[$d1['Id']] = $Value;
                                            }
                                        }
                                    }
                                }
                            }
                            if(count($ProfitLossDataArrayTemp)>0)
                            {
                                foreach($ProfitLossDataArrayTemp as $k1 => $v1) 
                                {
                                    #cross check if this accounts_value already exist--------
                                    $q = "SELECT Id,Value FROM client_group_profit_loss WHERE ClientGroupID = '".$d4['Id']."' AND AccountID = '".$k1."' AND Date = '".$v."'";
                                    $r = sqlsrv_query($conn,$q,array(), array("Scrollable" => 'static'));
                                    if(sqlsrv_num_rows($r)>0)
                                    {   
                                        $d = sqlsrv_fetch_array($r); 
                                        #update their value------------
                                        if($d['Value'] != $v1)
                                        {
                                            #update value if changed--------
                                            $q2 = "UPDATE client_group_profit_loss SET Value='".$v1."' UpdateOn = GETDATE() WHERE Id = '".$d['Id']."'";
                                            sqlsrv_query($conn,$q2);   
                                        }
                                    }
                                    else
                                    {
                                        $q2 = "INSERT INTO client_group_profit_loss(ClientGroupID,AccountID, Value, Date, AddOn) VALUES ('".$d4['Id']."','".$k1."','".$v1."','".$v."',GETDATE())";
                                        sqlsrv_query($conn,$q2); 
                                    }
                                }
                            }
                        } 
                    }       
                }
            }   
        }
    }
    #end client_group_profit_loss management---------------------
?>